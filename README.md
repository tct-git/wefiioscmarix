# WeFiLIbs

========================

__WeFiLibs__ is a framework which allows you to connect to WiFi Amenety and Premium Networks

- __Requirements__: iOS 12 or above
- __Swift version__: Swift 5.0.0 or above


## Usage

```
import WeFiLibs

// Get version
WeFiClient.version()

//Initial start of the framework
WeFiClient.shared.start()

//Request location permission - Usually start from here.
WeFiClient.shared.requestLocationPermissionMessageWithTitle( "Lorem Ipsum",
                                                             messageText: "Lorem Ipsum",
                                                             settingButtonTitle: "Settings",
                                                             cancelButtonTitle: "Cancel")


//For Measure network params implement install networkParamsDelegate in appropriate class 
WeFiClient.shared.networkParamsDelegate = self
// and use these methods
 WeFiClient.shared.startPing()
 WeFiClient.shared.startDownload()
 WeFiClient.shared.startUpload()


```


## Installation

### Swift Package Manager

1. Open your project with __Xcode__  `File -> Swift Packages -> Add package dependency` type your repository url. Go through all the steps. 
3. Go to __General__ pane of the application target in your project to the __Embedded Binaries__ section. Make sure  `WeFiLibs` is there. If not then add it. 
4. Go to your __Signing & Capabilities__ section of your project settings. Add such capabilities: `Access WiFi information`, `Network Extensions`: with App Proxy, Content Filter, Packet tunel, DNS proxy, `Background Modes`: with Location updates, Background Fetch
5. Go to __Info__ section - edit your Info.plist file. Add `Privacy - Location Always Usage Description - We provide WiFi profiles based on your location`, `Privacy - Location Always and When In Use Usage Description - We provide WiFi profiles based on your location updates`, `Privacy - Location When In Use Usage Description -We provide WiFi profiles based on your location`,  
     `Permitted background task scheduler identifiers` add item - `com.wefi.backgroundtaskidentifier`
     `App Transport Security Settings`- `Allow Arbitrary Loads` to YES
6. Open `AppDelegate` of your project.  Add `import WeFiLibs`. Make it inherited from `WeFiDelegate` like this:
```
import WeFiLibs

@main
class AppDelegate: WeFiDelegate

```
7. In any place you want the sdk start (user will be asked for location permission with this method firing) add `WeFiClient.shared.start()` like this: 
```
class ViewController: UIViewController {

override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    WeFiClient.shared.start()
}

```

## Release Notes

1.0.1 - Fixed automatic test when coming back from background.
Added methods for stop and cancel tests.
1.0.2 - Added new way of profile downloading. Now framework download and provide url to customer. File on this url is recommended to save to "Files" app. 
     - Automatic checking for new profiles on server. Client will be notified if profiles were updated. 



## License

© 2022 Wefi, LLC. All Rights Reserved.

